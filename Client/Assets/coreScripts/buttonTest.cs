﻿using UnityEngine;
using System.Collections;

public class buttonTest : MonoBehaviour {

	// Use this for initialization
    //void Start () {
    //    EventTriggerListener.Get(gameObject).onDown += OnDown;
    //    EventTriggerListener.Get(gameObject).onUp += OnUp;
    //}

    public void OnUp(GameObject go)
    {

        Debug.Log("OnUp Execute "+ go.name);
    }

    public void OnDown(GameObject go)
    {
        Debug.Log("OnDown Execute " + go.name);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
