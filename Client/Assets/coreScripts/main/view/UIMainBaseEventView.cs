﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace Assets.coreScripts.main.view
{
    public class UIMainBaseEventView:View
    {
        [Inject]
        public IEventDispatcher dispatcher { get; set; }

        public Action OnClose { get; set; }

        public virtual void OnInit(Action onCloseAction = null) { OnClose = onCloseAction; }

        public virtual void OnRender() { }

        public virtual void ClearGrid() { }

        public virtual void Close()
        {
            if (OnClose!=null)
            {
                OnClose.Invoke();
            } gameObject.SetActive(true);
        }
    }
}
