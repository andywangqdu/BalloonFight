﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace Assets.coreScripts.main.view
{
    public class UIMainBaseEventMediator : Mediator
    { 
		[Inject(ContextKeys.CONTEXT_DISPATCHER)]
		public IEventDispatcher dispatcher{ get; set;}

        protected virtual void UpdateListeners(bool enable)
        {

        }

        protected virtual void InitData() { }
    }
}
