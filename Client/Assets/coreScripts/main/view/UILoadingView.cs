﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.coreScripts.main.view
{
    public class UILoadingView:UIMainBaseEventView
    {
        public void LoadingScene() 
        {
            dispatcher.Dispatch(GameConfig.CoreEvent.LOADING_FIGHTING,"1");
        }
    }
}
