﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace Assets.coreScripts.main.view
{
    public class UILoadingMediator : UIMainBaseEventMediator
    {
        [Inject]
        public UILoadingView view { get; set; }
        public override void OnRegister()
        {
            UpdateListeners(true);
        }
        public override void OnRemove()
        {
            UpdateListeners(false);
        }

        public void LoadingScene(IEvent e)
        {
            dispatcher.Dispatch(GameConfig.CoreEvent.LOADING_FIGHTING, e.data);
        }
        
        protected override void UpdateListeners(bool enable)
        {
            view.dispatcher.UpdateListener(enable, GameConfig.CoreEvent.LOADING_FIGHTING, LoadingScene);
            base.UpdateListeners(enable);
        }
    }
}
