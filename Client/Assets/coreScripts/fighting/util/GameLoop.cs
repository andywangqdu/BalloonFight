﻿using System;
using System.Collections.Generic;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using UnityEngine;

namespace Assets.coreScripts.fighting.util
{
    public class GameLoop : MonoBehaviour,IGameTimer
    {
        private bool sendUpdates = false;

        [Inject(ContextKeys.CONTEXT_DISPATCHER)]
        public IEventDispatcher dispatcher { get; set; }

        #region IGameTimer Members

        public void Start()
        {
            sendUpdates = true;
        }

        public void Stop()
        {
            sendUpdates = false;
        }

        #endregion

        void Update() 
        {
            if (dispatcher != null && sendUpdates)
                dispatcher.Dispatch(GameConfig.CoreEvent.GAME_UPDATE);
        }
    }
}
