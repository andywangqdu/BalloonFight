﻿using System;
using System.Collections.Generic;

using Assets.coreScripts.fighting.model;
using strange.extensions.command.impl;

namespace Assets.coreScripts.fighting.controller
{
    public class OperationCommand : EventCommand
    {

        public override void Execute()
        {
            var cd = evt as CustomOperationEventData;

            switch (cd.operationEventType)
            {
                case GameConfig.OperationEvent.ONPRESS:
                    dispatcher.Dispatch(GameConfig.RoleEvent.PLAYER_MOVE, cd.dir);
                    break;
                case GameConfig.OperationEvent.ONRELEASE:
                    dispatcher.Dispatch(GameConfig.RoleEvent.PLAYER_CANCEL_MOVE, cd.dir);
                    break;
                default:
                    break;
            }
        }
    }
}
